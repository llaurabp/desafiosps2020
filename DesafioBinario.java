import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        int seq[];
        seq = new int [] {0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1};
        int[] aux = new int[seq.length];
        for (int i = 0; i < aux.length; i++)
        {
            aux[i] = -1;
        }

        System.out.println("Sequência inicial:");
        PrintArr(seq);

        for (int i = 0; i < seq.length; i++) {
            if (seq[i] == 1) continue;

            int[] cpy = (int[])seq.clone();
            cpy[i] = 1;

            int c1s = MostConsecutive1s(cpy);
            aux[i] = c1s;
        }


        int maxIndex = 0;
        for (int i = 0; i < aux.length; i++)
        {
            if (aux[i] > aux[maxIndex])
                maxIndex = i;
        }

        System.out.println("Índice: " + (maxIndex + 1));
        int[] exCpy = (int[]) seq.clone();
        exCpy[maxIndex] = 1;
        PrintArr(exCpy);
    }

    public static int[] RandomSequence(int length)
    {
        int[] arr = new int[length];

        for (int i = 0; i < length; i++)
            arr[i] = (int) Math.round(Math.random()* 1);

        return arr;
    }

    static void PrintArr(int[] seq)
    {
        for (int i : seq)
        {
            System.out.println(i);
        }
        System.out.println();
    }

    public static int MostConsecutive1s(int[] sequence)
    {
        int count = 0;
        int maxCount = 0;
        for (int num : sequence)
        {
            if (num == 1)
            {
                count++;
                if (count > maxCount)
                    maxCount = count;
            }
            else
            {
                count = 0;
            }
        }

        return maxCount;
    }
    }




