import com.sun.deploy.cache.BaseLocalApplicationProperties;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static <array> void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o número de funcionários:");
        int Numfuncionarios = scanner.nextInt();

        ArrayList<Cargo> cargos = new ArrayList<>();
        ArrayList<Funcionario> funcionarios = new ArrayList<>();


        int cont0 = 0;
        int cont1 = 0;
        int cont2 = 0;
        int cont3 = 0;
        int cont4 = 0;


        for (int i = 0; i < Numfuncionarios; i++) {
            System.out.println("Digite o nome do funcionário [" + (i + 1) + "]");
            String nome = scanner.next();

            System.out.println("Digite o código do cargo [" + (i + 1) + "]");
            int codigoCargo = scanner.nextInt();

            switch (codigoCargo) {
                default:
                    break;
                case 0:
                    cargos.add(new Cargo(codigoCargo, 2500));
                    break;
                case 1:
                    cargos.add(new Cargo(codigoCargo, 1500));
                    break;
                case 2:
                    cargos.add(new Cargo(codigoCargo, 10000));
                    break;
                case 3:
                    cargos.add(new Cargo(codigoCargo, 1200));
                    break;
                case 4:
                    cargos.add(new Cargo(codigoCargo, 800));
                    break;
            }

            int salario = cargos.get(i).getValor();

            while (true) {
                boolean igual = false;
                System.out.println("Digite o código do funcionário [" + (i + 1) + "]");
                int codigo = scanner.nextInt();
                for (int j = 0; j < funcionarios.size(); j++) {
                    if (codigo == funcionarios.get(j).getCodigo()) {
                            System.out.println("Código já existente, por favor digite novamente: ");
                        igual = true;
                    }
                }
                if (igual == false) {
                  funcionarios.add(new Funcionario(nome, codigo, codigoCargo, salario));
                    break;
                }
            }

            if (codigoCargo == 0) {
                cont0++;
            } else
            if (codigoCargo == 1) {
                cont1++;
            } else
            if (codigoCargo == 2) {
                cont2++;
            } else
            if (codigoCargo == 3) {
                cont3++;
            } else
            if (codigoCargo == 4) {
                cont4++;
            }

        }

        for (Funcionario i : funcionarios) {
            System.out.println(i);
        }

        int somaCont0 = cont0;
        int somaCont1 = cont1;
        int somaCont2 = cont2;
        int somaCont3 = cont3;
        int somaCont4 = cont4;
        if (somaCont0 > 0) {
            System.out.println("Valor pago aos funcionários do Cargo [0]: " + (somaCont0 * 2500));
        }
        if (somaCont1 > 0) {
            System.out.println("Valor pago aos funcionários do Cargo [1]: " + (somaCont1 * 1500));
        }
        if (somaCont2 > 0) {
            System.out.println("Valor pago aos funcionários do Cargo [2]: " + (somaCont2 * 10000));
        }
        if (somaCont3 > 0) {
            System.out.println("Valor pago aos funcionários do Cargo [3]: " + (somaCont3 * 1200));
        }
        if (somaCont4 > 0) {
            System.out.println("Valor pago aos funcionários do Cargo [4]: " + (somaCont4 * 800));
        }

}
}