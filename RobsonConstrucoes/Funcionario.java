public class Funcionario {


    public String nome;
    public int codigo, cargo, salario;


    public Funcionario(String nome, int codigo, int cargo, int salario) {
        this.nome = nome;
        this.codigo = codigo;
        this.cargo = cargo;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Funcionário{" +
                "Nome - " + nome +
                "; Código do funcionário = " + codigo +
                "; Código do cargo = " + cargo +
                "; Salário do funcionário = " + salario +
                "}";
    }
}
