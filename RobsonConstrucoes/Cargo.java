public class Cargo {

    public int codigo, valor;

    public Cargo() {}

    public Cargo(int codigo, int valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }


    @Override
    public String toString() {
        return "Cargo{" +
                "Código do cargo = " + codigo +
                "; Valor do salário = " + valor +
                "}";
    }
}
