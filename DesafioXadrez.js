
    //the function below will receive a chess board which the number of slots
    //is determined by the user, which normally is 64 slots (8 x 8)
    function xadrez(column, line) {
      //the array will receive data from columns and lines of the board
      let array = [
      [4, 3, 2, 5, 6, 2, 3, 4],
      [1, 1, 1, 1, 1, 1, 1, 1],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 1, 1, 1],
      [4, 3, 2, 5, 6, 2, 3, 4]
        ]
  
      //everytime the 'for' below repeats, it will create another empty array 'line'
      for (let a = 0; a < line; a++) {
        let linha = [];
        array.push(line);
        for (let b = 0; b < column; b++) {
          //'let column' will generate numbers between 0 and 6:
          //Math.floor will round the numbers and Math.random will generate numbers
          //between 0 and 1, so we multiply it by '7' so the results will be between 0 and 6
  
          let coluna = Math.floor(Math.random() * 7);
          array[a].push(column);
        }
      }
      
      //count-pieces:
      //create 'let pieces = []'
      let pieces = [];
      for (let a = 0; a < 8; a++) {
        pieces.push(0);
      }
  
      //check values in each column and line - they are between 0 and 6
      //everytime the value is returned, increment 1 (a++ and b++)
      for (let a = 0; a < array.length; a++) {
        for (let b = 0; b < array[a].length; b++) {
          pieces[array[a][b]]++;
        }
      }
      //creating variables representing the possibility of the pieces
      //that are assigned to a index
      //each variable has a value already
  
      empty = pieces[0];
      pawn = pieces[1];
      bishop = pieces[2];
      knight = pieces[3];
      rook = pieces[4];
      queen = pieces[5];
      king = pieces[6];
      //now we return the value of the pieces
      console.log("Empty slots: " + empty);
      console.log("Pawns: " + pawn);
      console.log("Bishops: " + bishop);
      console.log("Knights: " + knight);
      console.log("Rooks: " + rook);
      console.log("Queens: " + queen);
      console.log("Kings: " + king);
    }
    xadrez(8, 8);
