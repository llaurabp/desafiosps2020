public class Main {

        public static int ProdMax(int[] A)
        {

            //criando as duas variáveis pra armazenar o produto máximo e o mínimo
            int maximo = 0, minimo = 0;

            // armazenando o maximo produto até o momento
            int max_atual = 0;

            // percorrendo o array dado
            for (int i: A)
            {
                int temp = maximo;

                // atualizar máximo com máximo atual
                maximo = Integer.max(i, Integer.max(i * maximo,
                        i * minimo)
                );

                // armazenar minimo
                minimo = Integer.min(i, Integer.min(i * temp, i * minimo));

                max_atual = Integer.max(max_atual, maximo);
            }

            // retornar produto máximo
            return max_atual;
        }

        public static void main(String[] args)
        {
            int[] Array = { -6, 4, -5, 8, -10, 0, 8};

            System.out.print("Produto máximo: " + ProdMax(Array));

        }
    }

